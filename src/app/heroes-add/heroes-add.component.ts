import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { HeroService } from '../hero.service';
import {FormControl, Validators} from '@angular/forms';
import { HeroesListComponent } from '../heroes-list/heroes-list.component';


@Component({
  selector: 'app-heroes-add',
  templateUrl: './heroes-add.component.html',
  styleUrls: ['./heroes-add.component.css']
})
export class HeroesAddComponent implements OnInit {
  email = new FormControl('', [Validators.email]);

  getErrorMessage() {
    return this.email.hasError('email') ? 'Not a valid email' :
            '';
  }
  
  constructor(private heroservice:HeroService, private heroList: HeroesListComponent ){}

  ngOnInit() {

}
   addHero(name: string,email:string ){
     var hero={"id":0,"name":name,"email":email,"listAbility":null}
     return this.heroservice.addHero(hero).subscribe(data => this.heroList.ngOnInit());

   }
     
  }