import { Component, OnInit } from '@angular/core';
import { Hero } from '../hero';
import { Route } from '@angular/compiler/src/core';
import { ActivatedRoute } from '@angular/router';
import { HeroService } from '../hero.service';
import {Location} from '@angular/common';
import { Ability } from '../ability';
import { AbilityService } from '../ability.service';





@Component({
  selector: 'app-heroes-dettagli',
  templateUrl: './heroes-dettagli.component.html',
  styleUrls: ['./heroes-dettagli.component.css']
})


export class HeroesDettagliComponent implements OnInit {
  hero:Hero
  listAbility: Ability[];

  constructor(
  private  route : ActivatedRoute,
  private  heroService  : HeroService, 
  private  location : Location,
  private abilityService : AbilityService,
  ) { }

  ngOnInit() {
    this.getHero()
    this.getAbility()
  }
  
  getHero () {
    const id = +this.route.snapshot.paramMap.get('id');
    this.heroService.getHero(id)
    .subscribe(data => this.hero=data)
  }

  getAbility(){
    this.abilityService.getAbility().subscribe (data => this.listAbility = data)
  }

  emailUpdate (hero:Hero){
    this.heroService.update(hero).subscribe(data => this.ngOnInit())
    
  }

  addAbility(potere :string, descrizione:string )   {
    const ability = {"id":0, "potere":potere,"descrizione":descrizione,"listHero":null}
    this.abilityService.addAbility(ability).subscribe(data => this.ngOnInit())
  }

  addAbilityHero(id:number, ability:Ability){
    this.abilityService.addAbilityHero(id, ability).subscribe(data => this.ngOnInit());
  }

  deleteAbility(id:number){
    this.abilityService.deleteAbility(id).subscribe(data => this.ngOnInit() );
  }
}






