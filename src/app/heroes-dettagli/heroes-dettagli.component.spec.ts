import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeroesDettagliComponent } from './heroes-dettagli.component';

describe('HeroesDettagliComponent', () => {
  let component: HeroesDettagliComponent;
  let fixture: ComponentFixture<HeroesDettagliComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeroesDettagliComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeroesDettagliComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
