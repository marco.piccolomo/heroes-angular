import { NgModule, Component } from '@angular/core';
import { HeroesListComponent } from './heroes-list/heroes-list.component';
import { Routes, RouterModule } from '@angular/router';
import { HeroesDettagliComponent } from './heroes-dettagli/heroes-dettagli.component';
import { HeroesAddComponent } from './heroes-add/heroes-add.component';

const routes: Routes = [
  { path: 'heroes', component: HeroesListComponent },
  { path: 'details/:id', component: HeroesDettagliComponent },
  { path: 'add', component: HeroesAddComponent },
  { path : '', redirectTo: '/heroes', pathMatch:'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]

})
export class AppRoutingModule { }

