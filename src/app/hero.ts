import { Ability } from "./ability";

export class Hero {

    id: number;
    name: string;
    email: string;
    listAbility: Ability[];
}


