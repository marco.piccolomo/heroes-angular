import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Hero } from './hero';



const httpOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  private url = "/hero"

  constructor(private http: HttpClient) {

  }
  getHero(id: number): Observable<Hero> {
    return this.http.get<Hero>(this.url + "/byid/" + id)
  }

  getHeroes(): Observable<Hero[]> {
    return this.http.get<Hero[]>(this.url + "/all")
  }

  addHero(hero: Hero): Observable<Hero> {
    //alert(name);
    return this.http.post<Hero>(this.url + "/add", hero, httpOption);


  }

  delete(hero: Hero): Observable<Hero> {
    return this.http.delete<Hero>(this.url + "/delete/" + hero.id)
  }

  update(hero: Hero):Observable<Hero>{
    return this.http.post<Hero>(this.url + "/update",hero, httpOption );
  }




} 