import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Ability } from './ability';
import { Observable } from 'rxjs';

const httpOption = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AbilityService {

  private url = "/hero"

  constructor(private http: HttpClient) {
  }

  addAbility(ability: Ability): Observable<Ability> {
    return this.http.post<Ability>(this.url + "/addAbility", ability, httpOption);
  }

  getAbility(): Observable<Ability[]> {
    return this.http.get<Ability[]>(this.url + "/ability")
  }

  addAbilityHero(id: number, ability: Ability): Observable<Ability> {
    return this.http.post<Ability>(this.url + "/addAbilityHero/" + id, ability, httpOption);
  }

   deleteAbility(id:number):Observable<Ability>{
     return this.http.delete<Ability>(this.url + "/deleteAbility/"+ id)
   }
}